def produs(a,b):
    p = 0
    s = 0
    while a > 0:
        p = p + 1
        s = s + b * (a % 2)
        a = a / 2
        b = b * 2
    return s
a=3
b=9
print a, ' * ', b, ' = ', produs(a,b)