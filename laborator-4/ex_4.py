def invers_cifre(n):
    m = 0
    while n > 0:
        m = m * 10 + n % 10
        n = n / 10
    return m

n=4321
print 'Inversul numarului ', n, ' este ', invers_cifre(n)
