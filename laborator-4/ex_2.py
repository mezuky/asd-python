def polinom(a, x):
    n = len(a) - 1
    v = a[n]
    i = n
    while i >= 0:
        i = i - 1
        v = v * x + a[i]
    return v

a=[3,4,5,7]
x=2
print 'Valoarea polinomului, p, cu coeficienti ', a, ' in punctul x=', 2, ' este ', polinom(a,x)