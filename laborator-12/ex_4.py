def identific(p):
    n = len(p)
    i=n-1
    while i>0 and p[i]<p[i-1]:
        i=i-1
    return i

def minim(p,i):
    k=i
    for j in range(i+1,len(p)):
        if p[j]<p[k] and p[j]>p[i-1]:
            k = j
    return k

def inversare(p,i,n):
    k=i
    h=n-1
    while k<h:
        p[k],p[h]=p[h],p[k]
        k += 1
        h -= 1
    return p

def perm(n):
    p = range(1,n+1)
    print p
    i = identific(p)
    while i>0:
        kmin = minim(p, i)
        p[i-1],p[kmin]=p[kmin],p[i-1]
        p = inversare(p,i,n)
        print p
        i = identific(p)


perm(4)