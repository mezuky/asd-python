def f(x):
    return x*x-3
def bisectie(a,b, epsilon):
    s=a
    d=b
    while abs(d-s)>epsilon:
        m = (s+d)/2.
        if f(m)==0: return m
        elif f(m)*f(s)<0: d=m
        else: s=m
    return (d+s)/2

a=0
b=3
print 'functia f(x)=x^2-3 are in intervalul [',a,',',b,'] radacina ',  bisectie(a,b,0.002)