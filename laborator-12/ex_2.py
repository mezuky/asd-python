def cautare_binara(x,s,d,v):
    if s>d: return False
    else:
        m = (s+d) / 2
        if v==x[m]: return True
        elif v < x[m]: return cautare_binara(x,s,m-1,v)
        else: return cautare_binara(x,m+1,d,v)

x = [ 1,2,3,4,6,7,9,11,23,45]
v=35
print 'Elementul ', v, 'se afla in tabloul ', x, '?', cautare_binara(x, 0, len(x)-1, v)
