def power(x,n):
    if n==1: return x
    if n==2: return x*x
    else:
        aux = power(x, n/2)
        if n%2==1:
            return x*aux*aux
        else:
            return aux*aux

n=3
x=4
print x, '^', n, '=', power(x,n)