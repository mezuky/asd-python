def adunare_matrici(a,b):
    n = len(a)
    m = len(a[0])
    c = [[0]*m for i in range(n)]
    for i in range(n):
        for j in range(m):
            c[i][j] = a[i][j] + b[i][j]
    return c

a=[[3, 1, 6], [1, 8, 12]]
b=[[11, 7, 3], [12, 12, 12]]

print 'a=', a
print 'b=', b
print 'a+b=', adunare_matrici(a,b)