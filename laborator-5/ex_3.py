def simetrica(a):
    n = len(a)
    i = 0
    matrice_simetrica = True
    while i < n-1 and matrice_simetrica:
        j = i + 1
        while j < n and matrice_simetrica:
            if a[i][j] != a[j][i]:
                matrice_simetrica = False
            j = j + 1
        i = i + 1
    return matrice_simetrica

a=[[4, 8, 5], [7, 1, 6], [4,6,7]]
print 'Matricea ', a, ' este simetrica?', simetrica(a)

a=[[4, 2, 5], [8, 1, 3], [5,7,5]]
print 'Matricea ', a, ' este simetrica?', simetrica(a)