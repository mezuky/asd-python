def produs(a,x):
    m = len(a)
    n = len(a[0])
    c = [0]*m
    for i in range(m):
        for j in range(n):
            c[i] = c[i] + a[i][j] * x[j]
    return c

a=[[4, 8, 5], [7, 1, 6]]
b=[2,8,4]
print 'a=', a
print 'x=', b
print 'a*x=', produs(a,b)