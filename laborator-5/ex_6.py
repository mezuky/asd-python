from math import sqrt

def prim(n):
    nr_prim = True
    divizor = 2
    while nr_prim and divizor <= sqrt(n):
        if n % divizor == 0:
            nr_prim = False
        divizor = divizor + 1
    return nr_prim

def generare(n):
    k = 0
    p=[]
    for i in range(2, n+1):
        if prim(i):
            p.append(i)
    return p

n=100
print 'Sirul de nr prime mai mici decat ', n, ' fara ciur ', generare(n)