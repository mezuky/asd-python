def secventa(a):
    n = len(a)
    max = 0
    imax = 0
    jmax = -1
    for i in range(n):
        s = 0
        for j in range(i, n):
            s = s + a[j]
            if s > max:
                max = s
                imax = i
                jmax = j
    return max, imax, jmax

a=[2,4,6,-2,5,2,-78,3,5,7,2,1]
max, imax, jmax = secventa(a)
print 'Secventa de suma maxima din sirul ', a, ' este ', a[imax:jmax+1], ' cu suma ', max