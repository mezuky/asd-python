def ordonat(a):
    n = len(a)-1
    r = True
    i = -1
    while i < n-1 and r:
        i = i + 1
        if a[i] > a[i+1]:
            r = False
    return r

a=[3,5,18,19,25]
print 'Tablou ', a, ' este ordonat crescator?', ordonat(a)

a=[3,5,8,9,23,1]
print 'Tablou ', a, ' este ordonat crescator?', ordonat(a)