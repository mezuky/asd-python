def sumProdTablou(a):
    s = 0
    p = 1
    for i in range(0, len(a)):
        if a[i] % 2 == 0:
            s = s + a[i]
        else:
            p = p * a[i]
    return s, p

a = [1,5,0,7,3,6,9,10]
sum, prod = sumProdTablou(a)
print 'Suma elementelor de pe pozitile pare ale sirului ', a, ' este ', sum
print 'Produsul elementelor de pe pozitile impare ale sirului ', a, ' este ', prod