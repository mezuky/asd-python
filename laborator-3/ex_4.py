def generatorVector(seed,n):
    a=1103515245
    b=12345
    c=1
    rez=[]
    for i in range(32):
        c=c*2
    x=seed
    for i in range(n):
        x=(a*x+b)%c
        rez.append(int(x%6+1))
    return rez

def frecventa(lista):
    f=[0]*6
    for i in range(0,len(lista)):
        f[lista[i]-1]=f[lista[i]-1]+1
    for i in range(0,6):
        f[i]=(1.0*f[i])/len(lista)
    return f


rez=generatorVector(355,200)
print 'vectorul este: ', rez
f= frecventa(rez)
print 'distributia frecventelelor este: ', f