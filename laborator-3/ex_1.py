def tablouOrdonat(a):
    suntOrdonate = True
    i = 0
    while suntOrdonate and i < len(a) - 1:
        if a[i] > a[i+1]:
            suntOrdonate = False
        i = i + 1
    return suntOrdonate

a = [6, 1, 2, 3, 4, 5, 6]
print 'Tablou ', a, ' este ordonat?', tablouOrdonat(a)