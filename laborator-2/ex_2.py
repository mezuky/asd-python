def factoriPrimi(n):
    divizor=2
    while n>=divizor:
        putere=0
        while n % divizor == 0:
            putere = putere + 1
            n = n / divizor
        if putere != 0:
            print divizor, '^', putere
        divizor = divizor + 1

print 'Descompunerea in factori primi a lui  ', 12, ' este '
factoriPrimi(12)