import math

def numarPrim(n):
  nrPrim=True
  divizor = 2
  while nrPrim and divizor <= math.sqrt(n):
    if n % divizor ==0:
      nrPrim = False
    divizor = divizor +1
  return nrPrim

print 'Este prim ', 3, '?', numarPrim(3)