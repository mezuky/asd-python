def aproximareSuma(epsilon):
    n=1
    t_curent = 2
    t_anterior = 1
    while abs(t_curent-t_anterior) > epsilon:
        t_anterior = t_curent
        n = n + 1
        aux = 1 + 1./n
        t_curent = aux
        for i in range(2, n+1):
            t_curent = t_curent * aux
    return t_curent

print 'Aproximare cu epsilon=', 0.0001, ' lim= ', aproximareSuma(0.0001)