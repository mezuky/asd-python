def counting(x,m,c):
    n = len(x)
    m = m + 1
    f=[0]*m
    y=[0]*n
    putere = 10**c
    for i in range(n):
        j = (x[i]/putere) % 10
        f[j]+=1
    for i in range(1,m):
        f[i]=f[i-1]+f[i]
    for i in range(n-1,-1,-1):
        j = (x[i]/putere) % 10
        y[f[j]-1]=x[i]
        f[j]-=1
    for i in range(n):
        x[i] = y[i]
    return x

def radix_sort(x, k):
    for i in range(k):
        x = counting(x,9,i)
    return x

a = [1234,34,567,333,223567,343]
print 'Tabloul ', a, ' sortat este', radix_sort(a, 6)