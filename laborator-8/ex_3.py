def shakersort(x):
    n = len(x)
    s = 0
    d = n-1
    t=1
    while (t!=0 and s!=d):
        t=0
        for i in range(s,d):
            if x[i]>x[i+1]:
                x[i],x[i+1]=x[i+1],x[i]
                t=i
        if t != 0:
            d = t
            t = 0
            for i in range(d,s,-1):
                if x[i]<x[i-1]:
                    x[i],x[i-1]=x[i-1],x[i]
                    t=i
            s=t
    return x

x=[3,5,7,2,4,6,8,9,1,2]
print 'Tabloul ', x, ' sortat este ', shakersort(x)