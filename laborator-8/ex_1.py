def counting(x,m):
    n = len(x)
    m = m + 1
    f=[0]*m
    y=[0]*n
    for i in range(n):
        f[x[i]]+=1
    for i in range(1,m):
        f[i]=f[i-1]+f[i]
    for i in range(n-1,-1,-1):
        y[f[x[i]]-1]=x[i]
        f[x[i]]-=1
    for i in range(n):
        x[i] = y[i]
    return x

a = [3,6,9,1,3,5,7,3,2,4,7]
print 'Tabloul ', a, ' sortat este', counting(a, max(a))