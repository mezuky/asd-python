class Nod:
    def __init__(self, x, urm):
        self.info = x
        self.urm = urm
    def __repr__(self):
        return str(self.info) + " "

class ListaSimpluInlantuita:
    def __init__(self):
        self.inceput = None

    def adaugareFata(self, x):
        if self.inceput == None:
            self.inceput = Nod(x, None)

        else:
            nodNou = Nod(x, self.inceput)
            self.inceput = nodNou

    def adaugareSpate(self, x):
        if self.inceput == None:
            self.inceput = Nod(x, None)
            self.sfarsit = self.inceput
        else:
            aux = self.inceput
            while aux.urm != None:
                aux = aux.urm
            nodNou = Nod(x, None)
            aux.urm = nodNou

    def __repr__(self):
        aux = self.inceput
        r="["
        while aux != None:
            r += str(aux.info) + ", "
            aux = aux.urm
        if len(r) != 1:
            r = r[:len(r)-2]
        r+="]"
        return r

    def stergereFata(self):
        if self.inceput != None:
            self.inceput = self.inceput.urm

    def stergereSpate(self):
        if self.inceput == None:
            return
        elif self.inceput.urm == None:
             self.inceput = None
        else:
            aux = self.inceput
            while aux.urm.urm != None:
                aux = aux.urm
            aux.urm = None


    def stergereDupaElement(self, x):
        aux = self.inceput
        while aux != None and aux.info != x:
            aux = aux.urm
        if aux != None and aux.urm != None:
            aux.urm = aux.urm.urm
        else:
            print ("Elementul ", x, "nu este in lista")
            return

    def adaugareDupaElement(self, x, y):
        aux = self.inceput
        while aux != None and aux.info != x:
            aux = aux.urm
        if (aux != None):
            nodNou = Nod(y, aux.urm)
            aux.urm = nodNou
        else:
            print ("Elementul ", x, "nu este in lista")
            return

    def cautareaUnuiElementInLista(self, x):
        aux = self.inceput
        while aux != None and aux.info != x:
            aux = aux.urm
        return aux != None


    def cautareaUnuiElementInListaDupaIndex(self, x, k):
        aux = self.inceput
        i = 0
        while aux != None and i<k:
            aux = aux.urm
            i += 1
        return aux != None



l = ListaSimpluInlantuita()
l.adaugareSpate(5)
print (l)
l.adaugareSpate(6)
print (l)
l.stergereSpate()
l.stergereSpate()
l.stergereSpate()
print (l)
l.adaugareFata(4)
l.adaugareFata(6)
l.adaugareFata(5)

print ("Cautare elemnt", l.cautareaUnuiElementInLista(5))
print ("Elemntul de pe pozitia 2", l.cautareaUnuiElementInListaDupaIndex(2))