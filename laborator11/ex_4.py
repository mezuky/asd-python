class Nod:
    def __init__(self, x, prec, urm):
        self.info = x
        self.urm = urm
        self.prec = prec
    def __repr__(self):
        return str(self.info) + " "

class ListaDubluInlantuita:
    def __init__(self):
        self.inceput = None
        self.sfarsit = None



    def adaugareFata(self, x):
        if self.inceput == None:
            ref = Nod(x, None, None)
            self.inceput = self.sfarsit = ref
        else:
            ref = Nod(x, None, None)
            ref.urm = self.inceput
            self.inceput.prec =ref
            self.inceput = ref

    def adaugareSpate(self, x):
        if self.inceput == None:
            ref = Nod(x, None, None)
            self.inceput = self.sfarsit = ref
        else:
            ref = Nod(x, None, None)
            ref.prec = self.sfarsit
            self.sfarsit.urm =ref
            self.sfarsit = ref

    def inserareDupaElement(self, adr, y):
        ref = Nod(y, None, None)
        ref.prec = adr
        ref.urm = adr.urm
        adr.urm = ref
        ref.urm.prec = ref

    def inserareImainteElement(self, adr, y):
        ref = Nod(y, None, None)
        ref.urm = adr
        ref.prec = adr.prec
        adr.prec = ref
        ref.prec.urm = ref

    def stergeFata(self):
        if self.inceput != None:
            if self.inceput == self.sfarsit:
                self.inceput = self.safarsit = None
            else:
                self.inceput = self.inceput.urm
                self.inceput.prec = None
        else:
            print ("Lista vida nu se poate sterge")
            return

    def stergeSpate(self):
        if self.inceput != None:
            if self.inceput == self.sfarsit:
                self.inceput = self.sfarsit = None
            else:
                self.sfarsit = self.sfarsit.prec
                self.sfarsit.urm = None
                print ("delete", self.sfarsit)
        else:
            print ("Lista vida nu se poate sterge")
            return

    def stergereElement(self, adr):
        if adr.urm != None:
            adr.urm.prec = adr.prec
        else:
            self.sfarsit = adr.prec
        if adr.prec != None:
            adr.prec.urm =adr.urm
        else:
            self.inceput = adr.urm


    def cautareaUnuiElementInLista(self, x):
        aux = self.inceput
        while aux != None and aux.info != x:
            aux = aux.urm
        return aux

    def __repr__(self):
        aux = self.inceput
        r="["
        while aux != None:
            r += str(aux.info) + ", "
            aux = aux.urm
        if len(r) != 1:
            r = r[:len(r)-2]
        r+="]"
        return r


l = ListaDubluInlantuita()
#l.adaugareSpate(1)

#l.adaugareSpate(0)

#l.adaugareSpate(2)

l.adaugareFata(1)
l.adaugareFata(0)
l.adaugareFata(2)
aux = l.cautareaUnuiElementInLista(0)
l.inserareDupaElement(aux,10)
l.inserareImainteElement(aux,9)
l.stergeFata()
l.stergeSpate()
#l.stergeSpate()

print(l)
aux = l.cautareaUnuiElementInLista(0)
l.stergereElement(aux)
print(l)