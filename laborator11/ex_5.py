class Nod:
    def __init__(self, c, p, urm):
        self.putere = p
        self.coeficient = c
        self.urm = urm
    def __repr__(self):
        return str(self.coeficient) + "X^" + str(self.putere)

class Polinom:
    def __init__(self):
        self.inceput = None


    def adaugare(self, c, p):
        if self.inceput == None:
            self.inceput = Nod(c, p, None)
        elif self.inceput.putere > p:
            nodNou = Nod(c, p, self.inceput)
            self.inceput = nodNou
        else:
            prev = aux = self.inceput
            while aux != None and aux.putere < p:
                prev = aux;
                aux = aux.urm
            nodNou = Nod(c, p, prev.urm)
            prev.urm = nodNou

    def __repr__(self):
        aux = self.inceput
        r=""
        while aux != None:
            r += str(aux) + " + "
            aux = aux.urm
        if len(r) != 1:
            r = r[:len(r)-2]
        return r

    def cautare(self, x):
        aux = self.inceput
        while aux != None and aux.putere != x:
            aux = aux.urm
        return aux

    def evaluare(self, X):
        r=0
        aux = self.inceput
        while aux != None:
            r += aux.coeficient*(X**aux.putere)
            aux = aux.urm
        return r

def adunare(p1, p2):
    r = Polinom()
    aux = p1.inceput
    while aux != None:
        r.adaugare(aux.coeficient, aux.putere)
        aux = aux.urm
    aux = p2.inceput
    while aux != None:
        f=r.cautare(aux.putere)
        if f != None:
            f.coeficient += aux.coeficient
        else:
            r.adaugare(aux.coeficient, aux.putere)
        aux = aux.urm
    return r

p1 = Polinom()
p1.adaugare(3,100)
p1.adaugare(5,1)
p1.adaugare(8,15)
p1.adaugare(4,1001)
p1.adaugare(7,24)

p2=Polinom()
p2.adaugare(5,1)
p2.adaugare(8,15)
p2.adaugare(8,16)
print (p1)
print(p2)
print (p1.evaluare(1))
print(adunare(p1,p2))