class Nod:
    def __init__(self, x, urm):
        self.info = x
        self.urm = urm
    def __repr__(self):
        return str(self.info) + " "

class ListaSimpluInlantuita:
    def __init__(self):
        self.inceput = None

    def adaugareSpate(self, x):
        if self.inceput == None:
            self.inceput = Nod(x, None)
            self.sfarsit = self.inceput
        else:
            aux = self.inceput
            while aux.urm != None:
                aux = aux.urm
            nodNou = Nod(x, None)
            aux.urm = nodNou

    def __repr__(self):
        aux = self.inceput
        r="["
        while aux != None:
            r += str(aux.info) + ", "
            aux = aux.urm
        if len(r) != 1:
            r = r[:len(r)-2]
        r+="]"
        return r


    def cautareaUnuiElementInLista(self, x):
        aux = self.inceput
        while aux != None and aux.info != x:
            aux = aux.urm
        return aux != None


def auxReuniune(l, r):
    aux = l.inceput
    while aux != None:
        if not r.cautareaUnuiElementInLista(aux.info):
            r.adaugareSpate(aux.info)
        aux = aux.urm
    return r

def reuniune(l1,l2):
    r = ListaSimpluInlantuita()
    r = auxReuniune(l1,r)
    r = auxReuniune(l2, r)
    return r

l1 = ListaSimpluInlantuita()
l1.adaugareSpate(5)
l1.adaugareSpate(4)
l1.adaugareSpate(5)

l2 = ListaSimpluInlantuita()
l2.adaugareSpate(1)
l2.adaugareSpate(3)
l2.adaugareSpate(4)

r = reuniune(l1,l2)
print ('Reuniune: ', r)

def split(l):
    aux = l.inceput
    l1 = ListaSimpluInlantuita()
    l2 = ListaSimpluInlantuita()
    i=1
    while aux != None:
        if i%2 ==0:
            l1.adaugareFata(aux.info)
        else:
            l2.adaugareFata(aux.info)
        aux = aux.urm
        i += 1
    return l1,l2

print ('Split: ', split(r))