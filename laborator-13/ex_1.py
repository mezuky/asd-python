def partitie(x,s,d,nrc):
    v=x[s]
    i=s-1
    j=d+1
    while i<j:
        nrc +=1
        i += 1
        while x[i]<v:
            i +=1
            nrc +=1
        nrc+=1
        j -= 1
        while x[j]>v:
            j-=1
            nrc+=1
        nrc+=1
        if i<j :
            x[i],x[j]=x[j],x[i]
    return j,nrc

def quicksort(x,s,d,nrc):
    nrc = nrc +1
    if (s<d):
        q,nrc=partitie(x,s,d,nrc)
        nrc=quicksort(x,s,q,nrc)
        nrc=quicksort(x,q+1,d,nrc)
    return nrc

x=[9,8,7,6,5,4,3,2,1,1,2,1,12,2,1]

print 'Nr comparari quicksort:', quicksort(x,0,len(x)-1,0)
print 'Sirul sortat ', x