def interclasare(x,s,d,m,nrc):
    i=s
    j=m+1
    k=0
    nrc+=1
    c=[0]*(d-s+1)
    while i<=m and j<=d:
        nrc+=1
        if x[i]<=x[j]:
            c[k]=x[i]
            i+=1
        else:
            c[k]=x[j]
            j+=1
        k += 1
    nrc +=1
    while i<=m:
        c[k]=x[i]
        i+=1
        k+=1
        nrc +=1
    nrc +=1
    while j<=d:
        nrc +=1
        c[k]=x[j]
        k+=1
        j+=1
    for i in range(s, d+1):
        x[i]=c[i-s]
    return nrc