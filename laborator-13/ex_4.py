def reuniune (a,b):
    c=[]
    i=0
    j=0
    m = len(a)-1

    if a[i] < b[j]:
        c.append(a[i])
        i += 1
    else:
        if a[i] > b[j]:
            c.append(b[j])
            j += 1
        else:
            c.append(a[i])
            i += 1
            j += 1
    k = 0
    while i <= m and j <= n:
        if a[i] < b[j]:
            if a[i] != c[k]:
                k = k + 1
                c.append(a[i])
                i = i + 1
            else:
                i = i + 1
        else:
            if a[i] > b[j]:
                if b[j] != c[k]:
                    k = k + 1
                    c.append(b[j])
                    j = j + 1
                else:
                    j = j + 1
            else:
                if a[i] != c[k]:
                    k = k + 1
                    c.append(a[i])
                    i = i + 1
                    j = j + 1
                else:
                    i = i + 1
                    j = j + 1

    while i <= m:
        if a[i] != c[k]:
            k = k + 1
            c.append(a[i])
            i = i + 1
        else:
            i = i + 1
    while j <= n:
        if b[j] != c[k]:
            k = k + 1
            c.append(b[j])
            j = j + 1
        else:
            j = j + 1
    return c

def intersectie(a,b):

    c=[]
    n = len(b) - 1
    m = len(a) - 1
    k=0
    i=0
    j=0

    while (a[i]!=b[j]) and (i<=m) and (j<=n):
        if (a[i]>b[j]):
            j = j + 1
        else:
            i = i + 1
    if (i<=m) and (j<=n):
        c.append(a[i])

    while (i<=m) and (j<=n):
        if a[i]==b[j]: # s-a gasit element comun
            if a[i]!=c[k]: # valoarea comuna nu apare inca in c
                k = k+1
                c.append(a[i])
            i=i+1 # se progreseaza in ambele tablouri
            j=j+1
        else:
            if (a[i]<b[j]): # valori diferite: se ignora
                i = i + 1
            else:
                j = j + 1
    return c

a = [ 1,2,3,4,6,8,9,11,12,14]
b = [1,3,5,6,7,10,12]
print 'Reuniunea ', a, 'si', b, 'este', reuniune(a,b)
print 'Intersectia ', a, 'si', b, 'este', intersectie(a,b)