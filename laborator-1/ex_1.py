def max_min(n1, n2):

    max = n1
    min = n1

    if n2 > max:
        max = n2

    if n2 < min:
        min = n2

    return max, min


max, min = max_min(4, 8)
print "max:", max
print "min:", min