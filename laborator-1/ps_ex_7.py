def f(x):
    if (x >= -3 and x < 10):
        return 3 * x + 6

    return x * x + 7

print f(3)