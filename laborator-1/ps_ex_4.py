def cm_to_inch(cm):
    return cm * 0.39

def celsius_to_fahrenheit(celsius):
    return (celsius * 1.8) + 32

print "15 cm in inch: ", cm_to_inch(15)
print "32 grade celsius in fahrenheit: ", celsius_to_fahrenheit(32)