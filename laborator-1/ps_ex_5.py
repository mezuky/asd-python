import math

def aria_hexagon(l):
    return ((3 * math.sqrt(3)) / 2) * (l*l)

print "aria unui hexagon cu latura 3 este ", aria_hexagon(3)