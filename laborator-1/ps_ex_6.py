def bmi(masa, inaltime):
    bmi = masa / (inaltime * inaltime)

    if (bmi < 18.5):
        return "Risc pentru sanatate: ridicat"

    if (bmi >= 18.5 and bmi <= 24.9):
        return "Risc pentru sanatate: minim/scazut"

    if (bmi >= 25 and bmi <= 29.9):
        return "Risc pentru sanatate: scazut/moderat"

    if (bmi >= 30 and bmi <= 34.9):
        return "Risc pentru sanatate: moderat/ridicat"

    if (bmi >= 35):
        return "Risc pentru sanatate: ridicat"


print bmi(73, 1.6)