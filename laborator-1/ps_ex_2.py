def calc(n1, n2, operator):
    if (operator == "-"):
        return n1-n2

    if (operator == "*"):
        return n1*n2

    if (operator == "/"):
        return n1/n2

    return n1+n2

print calc(1, 4, "+")
print calc(3, 3, "/")
print calc(3, 3, "*")
print calc(4, 3, "-")