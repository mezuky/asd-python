class Stiva:
    def __init__(self, nmax):
        self.data = [None]*nmax
        self.v = 0
        self.nmax=nmax
    def esteGoala(self):
        return self.v == 0
    def estePlina(self):
        return self.v == self.nmax
    def push(self, e):
        if self.estePlina():
            print ('Stiva este plina nu se mai pot adauga elemente')
            return
        self.data[self.v] = e
        self.v += 1
    def pop(self):
        if self.esteGoala():
            print ('Stiva este goala nu mai putem extrage elemente')
            return
        self.v -= 1
        return self.data[self.v]
    def __repr__(self):
        return "Stiva: " + str(self.data[:self.v])


def verificareHTML(listaTaguri):
    n = len(listaTaguri)
    s = Stiva(n)
    for i in range(n):
        tagCurent = listaTaguri[i]
        if tagCurent[1] != '/': #tag deschis il adaug in stiva
            s.push(tagCurent)
        else:
            el = s.pop()
            if el == None:
                print ('Fisierul HTML nu este valid')
                return
            m1 = len(el)
            m2 = len(tagCurent)
            if el[1:m1-1] != tagCurent[2:m2-1]:
                print ('Fisierul HTML nu este valid')
                return
    if s.esteGoala():
        print ('Fisierul HTML este valid')
    else:
        print ('Fisierul HTML nu este valid')

verificareHTML(['<html>', '</html>', '</html>'])
verificareHTML(['<html>','<body>','<input>','</input>','<input>','</input>','<ls>','<li>','</li>','<li>','</li>','</ls>','</body>','</html>'])