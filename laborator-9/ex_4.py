class CoadaCirculara1:
    def __init__(self, nmax):
        self.data=[None]*nmax
        self.s=0
        self.f=0
        self.nmax=nmax
        self.nre = 0
    def esteGoala(self):
        return self.nre==0
    def estePlina(self):
        return self.nre == self.nmax
    def adauga(self, el):
        if self.estePlina():
            print('Coada este plina nu mai se pot adauga elemente')
            return
        self.data[self.s]=el
        self.s = self._nextIndex(self.s)
        self.nre += 1
    def extrage(self):
        if self.esteGoala():
             print('Coada este gola nu mai se pot scoate elemente')
             return
        retVal = self.data[self.f]
        self.data[self.f]=None
        self.f = self._nextIndex(self.f)
        self.nre -= 1
        return retVal
    def __repr__(self):
        ret="Coada[";
        poz =self.f
        i=0
        while(i < self.nre):
            ret = ret+ " "+str(self.data[poz])
            poz = self._nextIndex(poz)
            i += 1
        return ret + "] -- Reprezentare interna: " +str(self.data) + " f:" +str(self.f)+" s:"+str(self.s)
    def _nextIndex(self, poz):
        return (poz+1)%self.nmax