class CoadaCirculara2:
    def __init__(self, nmax):
        self.nmax=nmax
        self.data=[None]*(nmax+1)#alocam spatiu pentru elementul fictiv
        self.s=-1
        self.f=0
    def esteGoala(self):
        return self._nextIndex(self.s) == self.f
    def estePlina(self):
        return self._nextIndex(self._nextIndex(self.s))==self.f
    def adauga(self, el):
        if self.estePlina():
            print('Coada este plina nu mai se pot adauga elemente')
            return
        self.s = self._nextIndex(self.s)
        self.data[self.s]=el
    def extrage(self):
        if self.esteGoala():
             print('Coada este gola nu mai se pot scoate elemente')
             return
        retVal = self.data[self.f]
        self.data[self.f]=None
        self.f = self._nextIndex(self.f)
        return retVal
    def __repr__(self):
        ret="Coada[";
        poz =self.f
        while(poz != self._nextIndex(self.s)):
            ret = ret+ " "+str(self.data[poz])
            poz = self._nextIndex(poz)
        return ret + "] -- Reprezentare interna: " +str(self.data) + " f:" +str(self.f)+" s:"+str(self.s)
    def _nextIndex(self, poz):
        if poz == self.nmax:
            return 0
        else:
            return poz+1

#N- numar elevi
#M -numar ales (M<N)
def josephus(N,M):
    c=CoadaCirculara2(N)
    for i in range(N):
        c.adauga(i+1)
    count = 0;
    N -= 1
    while(count<N):
        for i in range(M-1):
            c.adauga(c.extrage())
        c.extrage()
        count+=1

    print ('Elevul care va raspunde are numarul: ', c.extrage())

josephus(20,6)
josephus(20,10)
josephus(20,5)