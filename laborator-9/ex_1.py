class Stiva:
    def __init__(self, nmax):
        self.data = [None]*nmax
        self.v = 0
        self.nmax=nmax
    def esteGoala(self):
        return self.v == 0
    def estePlina(self):
        return self.v == self.nmax
    def push(self, e):
        if self.estePlina():
            print ('Stiva este plina nu se mai pot adauga elemente')
            return
        self.data[self.v] = e
        self.v += 1
    def pop(self):
        if self.esteGoala():
            print ('Stiva este goala nu mai putem extrage elemente')
            return
        self.v -= 1
        return self.data[self.v]
    def __repr__(self):
        return "Stiva: " + str(self.data[:self.v])

s = Stiva(3)
s.push(3)
print (s)
s.push(2)
print (s)
s.push(1)
print (s)
s.push(0)
print ('Elemetul scos este: ', s.pop())