def eliminare_duplicate(a):
    n = len(a)
    k = 0
    for i in range(n-1):
        if a[i] < a[i+1]:
            k = k + 1
            a[k] = a[i+1]
    return a[:k+1]

a=[1,1,1,2,3,4,4,4,5,5,6,7,8,9,9]
print "Taboul ", a, ' dupa eliminarea duplicatelor este ', eliminare_duplicate(a)
