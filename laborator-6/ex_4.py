def apartine(a,e):
    n = len(a)
    gasit = False
    i = 0
    while i<n and not gasit:
        if a[i] == e:
            gasit = True
        else:
            i = i + 1
    return gasit

def reuniunea(a,b):
    n = len(a)
    m = len(b)
    r=[]
    for i in range(n):
        r.append(a[i])
    for i in range(m):
        if not apartine(a,b[i]):
            r.append(b[i])
    return r

a=[1,4,7,9]
b=[1,2,3,4,8,9]
print 'Reuniunea multimilor ', a, ' si ', b, ' este ', reuniunea(a,b)